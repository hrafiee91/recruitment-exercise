<?php
namespace app\helpers;

use DateTime;

class PersonalCode
{
    public static function getAge($code)
    {
        $code = (string) $code;
        $g = substr($code,0,1);
        $y = substr($code,1,2);
        $m = substr($code,3,2);
        $d = substr($code,4,2);
        switch ($g) {
            case '1':
            case '2':
                $y = '18'.$y;
                break;
            case '3':
            case '4':
                $y = '19'.$y;
                break;
            case '5':
            case '6':
                $y = '20'.$y;
                break;
        }
        $birthday = new DateTime($y.'-'.$m.'-'.$d);
        $now = new DateTime(date("Y-m-d"));
        $interval = $birthday->diff($now);
        return $interval->y;
    }
}