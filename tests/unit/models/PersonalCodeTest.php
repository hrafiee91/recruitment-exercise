<?php

namespace tests\models;

use app\helpers\PersonalCode;

class PersonalCodeTest extends \Codeception\Test\Unit
{
    public function testAge()
    {
        expect(PersonalCode::getAge('35712270228'))->equals(61);
    }
}