<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <section id="top-nav" class="bg-black">
        <div class="container">
            <div class="pull-left text-white">
                <div class="text-bold padding-top-small padding-bottom-small">
                    klienditeenindus
                    <span class="margin-left-small"><span class="glyphicon glyphicon-phone" aria-hidden="true"></span> 1715</span>
                    <span class="margin-left-small"><span class="glyphicon glyphicon-time" aria-hidden="true"></span> E-P 9.00-21.00</span>
                </div>
            </div>
            <div class="pull-right text-white">
                <div class="text-bold padding-top-small padding-bottom-small">
                    Terre, Kauppo Kastujio
                    <button class="btn btn-sm btn-warning button button-warning margin-left-small"><span class="glyphicon glyphicon-off margin-right-small"></span> Logout</button>
                </div>
            </div>
        </div>
    </section>
    <section id="nav">
        <div class="container padding-top-medium padding-bottom-medium">
            <div class="row">
                <div class="col-sm-3">
                    <?=Html::img('@web/img/logo.png',['class'=>'img img-responsive'])?>
                </div>
                <div class="col-sm-8">
                    <ul class="list-inline">
                        <li><a href="#">Add <span class="glyphicon glyphicon-chevron-right "></span></li>
                        <li><a href="#">Here <span class="glyphicon glyphicon-chevron-right "></span></a></li>
                        <li><a href="#">Random <span class="glyphicon glyphicon-chevron-right "></span></a></li>
                        <li><a href="#">Links to our page <span class="glyphicon glyphicon-chevron-right "></span></a></li>
                    </ul>
                </div>
                <div class="col-sm-1 hidden-sm hidden-xs">
                    <span class="text-small">NO-pykass</span>
                </div>
            </div>
        </div>
    </section>
    <section id="menu">
        <div class="bg-gray">
            <div class="container">
                <ul class="list-inline">
                    <li><a href="#">My Actions</a></li>
                    <li><a href="<?=Yii::$app->url->toRoute('/loans')?>">Loans</a></li>
                    <li><a href="<?=Yii::$app->url->toRoute('/users')?>">Users</a></li>
                </ul>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <div class="card position-relative">
                <div class=" position-top-left">
                    <a href="#">
                        <?=Html::img('@web/img/back.png',['class'=>'img img-responsive'])?>
                    </a>
                </div>
                <div class="padding-large">
                    <p class="text-lead"><?=$this->title?></p>
                </div>
                <div class="padding-left-large padding-right-large">
                    <?= $content ?>
                </div>
            </div>
            <div class="margin-top-small">
                <button class="btn btn-warning button button-warning">VAATAN SALDOSEIS</button>
                <button class="btn btn-warning button button-danger margin-left-small">MAKSAN VOLGNESVUSE</button>
                <button class="btn btn-warning button button-danger margin-left-small">MAKSAN KOGU LAENU UHE MAKSEGA</button>
            </div>
        </div>
    </section>
</div>

<footer class="footer">
<!--  footer  -->
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
