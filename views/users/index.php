<?php
/* @var $this yii\web\View */
$this->title = 'Users';
?>
<?php
$userModel = new \app\models\User();
$columns = array_keys($userModel->attributes);
$columns[] = ['class' => 'yii\grid\ActionColumn'];
echo yii\grid\GridView::widget([
    'dataProvider' => $userProvider,
    'columns' => $columns,
]);
?>