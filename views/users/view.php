<?php
/* @var $this yii\web\View */
$this->title = 'Users | Show';
?>
<h1>User Show : <?=$object->id?> <br> <a href="<?=Yii::$app->url->toRoute('/users/edit/'.$object->id)?>" class="btn">Edit</a> <a href="<?=Yii::$app->url->toRoute('/users/remove/'.$object->id)?>" class="btn btn-error">remove</a></h1>

<div>
    <table class="table">
        <thead>
        <tr>
            <th>Attribute</th>
            <th>Value</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td><b>First Name</b></td>
                <td><?=$object->first_name?></td>
            </tr>
            <tr>
                <td><b>Last Name</b></td>
                <td><?=$object->last_name?></td>
            </tr>
            <tr>
                <td><b>Email</b></td>
                <td><?=$object->email?></td>
            </tr>
            <tr>
                <td><b>Personal Code</b></td>
                <td><?=$object->personal_code?></td>
            </tr>
            <tr>
                <td><b>Age </b></td>
                <td><?=app\helpers\PersonalCode::getAge($object->personal_code)?></td>
            </tr>
            <tr>
                <td><b>Phone</b></td>
                <td><?=$object->phone?></td>
            </tr>
            <tr>
                <td><b>Active</b></td>
                <td><?=$object->active == 1?'<span class="label label-success">yes</span>':'<span class="label label-error">no</span>'?></td>
            </tr>
            <tr>
                <td><b>Dead</b></td>
                <td><?=$object->dead == 0 ?'<span class="label label-success">no</span>':'<span class="label label-error">yes</span>'?></td>
            </tr>
            <tr>
                <td><b>Language</b></td>
                <td><?= strtoupper($object->lang) ?></td>
            </tr>
        </tbody>
    </table>
</div>