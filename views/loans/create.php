<?php
/* @var $this yii\web\View */
if(isset($loan))
    $this->title = 'Loans | Update :'.$loan->id;
else
    $this->title = 'Loans | Create';
?>
<h1><?=$this->title?></h1>
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'options' => ['class' => 'form-group'],
]) ?>
<?= $form->field($model, 'user_id',['labelOptions' => [ 'class' => 'form-label' ]])->textInput(['class'=>'form-input','value'=>@$loan->user_id]) ?>
<?= $form->field($model, 'amount',['labelOptions' => [ 'class' => 'form-label' ]])->textInput(['class'=>'form-input','value'=>@$loan->amount]) ?>
<?= $form->field($model, 'interest',['labelOptions' => [ 'class' => 'form-label' ]])->textInput(['class'=>'form-input','value'=>@$loan->interest]) ?>
<?= $form->field($model, 'duration',['labelOptions' => [ 'class' => 'form-label' ]])->textInput(['class'=>'form-input','value'=>@$loan->duration]) ?>
<?= $form->field($model, 'start_date',['labelOptions' => [ 'class' => 'form-label' ]])->textInput(['class'=>'form-input','value'=>@$loan->start_date]) ?>
<?= $form->field($model, 'end_date',['labelOptions' => [ 'class' => 'form-label' ]])->textInput(['class'=>'form-input','value'=>@$loan->end_date]) ?>
<?= $form->field($model, 'campaign',['labelOptions' => [ 'class' => 'form-label' ]])->textInput(['class'=>'form-input','value'=>@$loan->campaign]) ?>
<?= $form->field($model, 'status',['labelOptions' => [ 'class' => 'form-label' ]])->textInput(['class'=>'form-input','value'=>@$loan->status]) ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?php if(isset($loan)): ?>
                <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
            <?php else: ?>
                <?= Html::submitButton('Create', ['class' => 'btn btn-primary']) ?>
            <?php endif; ?>
        </div>
    </div>
<?php ActiveForm::end() ?>