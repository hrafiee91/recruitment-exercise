<?php
/* @var $this yii\web\View */
$this->title = 'Loans | Show';
?>
<h1>Loan Show : <?=$object->id?> <br> <a href="<?=Yii::$app->url->toRoute('/loans/edit/'.$object->id)?>" class="btn">Edit</a> <a href="<?=Yii::$app->url->toRoute('/loans/remove/'.$object->id)?>" class="btn btn-error">remove</a></h1>

<div>
    <table class="table">
        <thead>
        <tr>
            <th>Attribute</th>
            <th>Value</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td><b>User ID</b></td>
                <td><?=$object->user_id?></td>
            </tr>
            <tr>
                <td><b>Amount</b></td>
                <td><?=$object->amount?></td>
            </tr>
            <tr>
                <td><b>Interest</b></td>
                <td><?=$object->interest?></td>
            </tr>
            <tr>
                <td><b>Duration</b></td>
                <td><?=$object->duration?></td>
            </tr>
            <tr>
                <td><b>Start date</b></td>
                <td><?=$object->start_date?></td>
            </tr>
            <tr>
                <td><b>End date</b></td>
                <td><?=$object->end_date?></td>
            </tr>
            <tr>
                <td><b>Campaign</b></td>
                <td><?=$object->campaign?></td>
            </tr>
            <tr>
                <td><b>Status</b></td>
                <td><?=$object->status?></td>
            </tr>
        </tbody>
    </table>
</div>