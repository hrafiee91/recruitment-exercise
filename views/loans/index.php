<?php
/* @var $this yii\web\View */
$this->title = 'Loans';
?>
<?php
$loanModel = new \app\models\Loan();
$columns = array_keys($loanModel->attributes);
$columns[] = ['class' => 'yii\grid\ActionColumn'];
echo yii\grid\GridView::widget([
    'dataProvider' => $loanProvider,
    'columns' => $columns,
]);
?>