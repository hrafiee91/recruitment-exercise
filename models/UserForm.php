<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class UserForm extends Model
{
    public $first_name,$last_name,$phone,$email,$personal_code,$dead,$lang;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name','email','phone','personal_code','dead','lang'], 'required'],
            ['email', 'email'],
            ['personal_code', 'string','length'=>[12,12]],
            ['personal_code', 'number'],
            ['lang','string','length'=>[3,3]]
        ];
    }
}
