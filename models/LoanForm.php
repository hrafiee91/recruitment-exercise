<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoanForm extends Model
{
    public $user_id,$amount,$interest,$duration,$start_date,$end_date,$campaign,$status;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['user_id','amount','interest','duration','start_date','end_date','campaign','status'],'safe']
        ];
    }
}
