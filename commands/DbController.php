<?php
/**
 * Created by PhpStorm.
 * User: Hossein Rafiee - PC
 * Date: 8/20/2019
 * Time: 7:10 PM
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\User;
use app\models\Loan;


/**
 * Class DbController
 * @package app\commands
 */
class DbController extends Controller
{

    /**
     *  this command will be import users.json and loans.json to database
     * @throws \yii\db\Exception
     */
    public function actionSeed()
    {
        $userModel = new User();
        $userData = json_decode(file_get_contents(__DIR__.'/../users.json'), true);
        Yii::$app->db->createCommand()->batchInsert(User::tableName(), $userModel->attributes(), $userData)->execute();

        $loanModel = new Loan();
        $loanData = json_decode(file_get_contents(__DIR__.'/../loans.json'), true);
        Yii::$app->db->createCommand()->batchInsert(Loan::tableName(), $loanModel->attributes(), $loanData)->execute();
        echo "Database Seed has been successfully inserted \n";
    }
}