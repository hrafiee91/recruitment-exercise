<?php

namespace app\controllers;

use app\models\LoanForm;
use Yii;
use app\models\Loan;
use yii\data\ActiveDataProvider;

class LoansController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $query = Loan::find();

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                    'user_id' => SORT_ASC,
                    'amount' => SORT_ASC,
                ]
            ],
        ]);
        return $this->render('index',['loanProvider'=>$provider]);
    }

    public function actionView($id)
    {
        $this->layout = 'simple';
        $object = Loan::find()->where([
            'id' => $id
        ])->one();

        if(empty($object)){
            die('Loan cannot find');
        }

        return $this->render('view', ['object' => $object]);

    }

    public function actionCreate()
    {
        $this->layout = 'simple';

        $model = new LoanForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $loanModel = new Loan();
            $loanModel->user_id = $model->user_id;
            $loanModel->amount = $model->amount;
            $loanModel->interest = $model->interest;
            $loanModel->duration = $model->duration;
            $loanModel->start_date = $model->start_date;
            $loanModel->end_date = $model->end_date;
            $loanModel->campaign = $model->campaign;
            $loanModel->status = $model->status;
            if($loanModel->validate()) {
                $loanModel->save();
                return $this->redirect(['loans/view', 'id' => $loanModel->id]);
            }
            else
                die(json_encode($loanModel->errors));
        } else {
            return $this->render('create',['model'=>$model]);
        }
    }


    public function actionUpdate($id)
    {
        $this->layout = 'simple';

        $model = new LoanForm();
        $loanModel = Loan::find()->where([
            'id'=>$id,
        ])->one();

        if(empty($loanModel)){
            die('Loan cannot find');
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $loanModel->user_id = $model->user_id;
            $loanModel->amount = $model->amount;
            $loanModel->interest = $model->interest;
            $loanModel->duration = $model->duration;
            $loanModel->start_date = $model->start_date;
            $loanModel->end_date = $model->end_date;
            $loanModel->campaign = $model->campaign;
            $loanModel->status = $model->status;
            if($loanModel->validate()) {
                $loanModel->save();
                return $this->redirect(['loans/view', 'id' => $loanModel->id]);
            }
            else
                die(json_encode($loanModel->errors));
        } else {
            return $this->render('create', ['loan'=>$loanModel, 'model' => $model]);
        }
    }

    /**
     * @param $id
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $loanModel = Loan::find()->where([
            'id'=>$id
        ])->one();

        if(empty($loanModel)){
            die('Loan cannot find');
        }
        $loanModel->delete();
        echo "Loan ".$id." has been removed successfully";
        return;
    }

}
